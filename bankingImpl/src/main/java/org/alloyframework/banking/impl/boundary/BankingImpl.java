package org.alloyframework.banking.impl.boundary;

import org.alloyframework.banking.contract.boundary.Banking.AccountTransactionRequest;
import org.alloyframework.banking.contract.boundary.Banking.AccountTransactionResponse;
import org.alloyframework.banking.contract.boundary.Banking.Deposit;
import org.alloyframework.banking.contract.boundary.Banking.FrozenAccountException;
import org.alloyframework.banking.contract.boundary.Banking.Transfer;
import org.alloyframework.banking.contract.boundary.Banking.Withdraw;
import org.alloyframework.banking.contract.entitygateway.BankingAccountEntityGateway;
import org.alloyframework.banking.contract.model.BankingAccount;
import org.alloyframework.server.contract.annotation.BoundaryMethod;
import org.alloyframework.server.contract.boundary.BoundaryContext;

import javax.annotation.Nonnull;
import javax.transaction.InvalidTransactionException;

import static org.alloyframework.banking.contract.boundary.Banking.FROZEN_ACCOUNT_EXCEPTION;
import static org.alloyframework.banking.contract.boundary.Banking.INSUFFICIENT_FUNDS_EXCEPTION;
import static org.alloyframework.banking.contract.boundary.Banking.InsufficientFundsException;
import static org.alloyframework.banking.contract.boundary.Banking
        .NEGATIVE_OR_ZERO_AMOUNT_TRANSFER_EXCEPTION;
import static org.alloyframework.banking.contract.boundary.Banking.TransferRequest;
import static org.alloyframework.banking.contract.boundary.Banking.TransferResponse;

final class BankingImpl {
    private BankingImpl() {
    }

    @Nonnull
    @BoundaryMethod(Transfer.class)
    private static TransferResponse transfer(
            @Nonnull final BoundaryContext context,
            @Nonnull final TransferRequest requestModel)
            throws Exception {
        checkArguments(requestModel.getAmount());

        final Withdraw withdraw = context.getBoundary(Withdraw.class);
        final Deposit deposit = context.getBoundary(Deposit.class);

        final AccountTransactionRequest withdrawRequest
                = AccountTransactionRequest.builder()
                .accountID(requestModel.getSourceAccountID())
                .amount(requestModel.getAmount())
                .build();

        final AccountTransactionRequest depositRequest
                = AccountTransactionRequest.builder()
                .accountID(requestModel.getTargetAccountID())
                .amount(requestModel.getAmount())
                .build();

        final AccountTransactionResponse withdrawTransaction
                = withdraw.execute(context, withdrawRequest);

        final AccountTransactionResponse depositTransaction
                = deposit.execute(context, depositRequest);

        if (withdrawTransaction.getAmount() != depositTransaction.getAmount())
            throw new InvalidTransactionException();

        return TransferResponse.builder()
                .sourceAccountID(requestModel.getSourceAccountID())
                .targetAccountID(requestModel.getTargetAccountID())
                .amount(withdrawTransaction.getAmount())
                .build();
    }

    @Nonnull
    @BoundaryMethod(Withdraw.class)
    private static AccountTransactionResponse withdraw(
            @Nonnull final BoundaryContext context,
            @Nonnull final AccountTransactionRequest requestModel)
            throws Exception {
        checkArguments(requestModel.getAmount());

        final BankingAccountEntityGateway bankingAccountEntityGateway
                = context.getEntityGateway(BankingAccount.class, BankingAccountEntityGateway.class);

        final BankingAccount account
                = bankingAccountEntityGateway.find(requestModel.getAccountID());

        checkAccount(account);
        checkFunds(account, requestModel.getAmount());

        account.setFunds(account.getFunds() - requestModel.getAmount());

        return AccountTransactionResponse.builder()
                .accountID(requestModel.getAccountID())
                .amount(requestModel.getAmount())
                .build();
    }

    @Nonnull
    @BoundaryMethod(Deposit.class)
    private static AccountTransactionResponse deposit(
            @Nonnull final BoundaryContext context,
            @Nonnull final AccountTransactionRequest requestModel)
            throws Exception {
        checkArguments(requestModel.getAmount());

        final BankingAccountEntityGateway bankingAccountEntityGateway
                = context.getEntityGateway(BankingAccount.class, BankingAccountEntityGateway.class);

        try (final BankingAccount account = bankingAccountEntityGateway
                .find(requestModel.getAccountID())) {

            checkAccount(account);

            account.setFunds(account.getFunds() + requestModel.getAmount());

            return AccountTransactionResponse.builder()
                    .accountID(requestModel.getAccountID())
                    .amount(requestModel.getAmount())
                    .build();
        }
    }

    /**
     * Invariants for transaction amount on bank account.
     *
     * @param amount the amount to validate.
     *
     * @throws IllegalArgumentException
     *      when the amount is negative or zero.
     */
    private static void checkArguments(
            final double amount)
            throws IllegalArgumentException {
        if (amount <= 0.0)
            throw new IllegalArgumentException(
                    NEGATIVE_OR_ZERO_AMOUNT_TRANSFER_EXCEPTION);
    }

    /**
     * Invariants for transactions on bank accounts.
     *
     * @param bankingAccount the bank account to validate.
     *
     * @throws FrozenAccountException
     *      when the given account is frozen and therefor transactions are not
     *      allowed.
     */
    private static void checkAccount(
            @Nonnull final BankingAccount bankingAccount)
            throws FrozenAccountException {
        if (bankingAccount.isFrozen())
            throw new FrozenAccountException(FROZEN_ACCOUNT_EXCEPTION);
    }

    /**
     * Invariants for withdraw transactions on bank account.
     *
     * @param bankingAccount
     *      the bank account to validate.
     * @param minimumRequiredFunds
     *      the minimum required funds for the given account.
     *
     * @throws InsufficientFundsException
     *      when the funds plus the line of credit associated with the given
     *      account is smaller than the requested amount.
     */
    private static void checkFunds(
            @Nonnull final BankingAccount bankingAccount,
            final double minimumRequiredFunds)
            throws InsufficientFundsException {
        final double funds
                = bankingAccount.getFunds() + bankingAccount.getLineOfCredit();

        if (funds < minimumRequiredFunds)
            throw new InsufficientFundsException(INSUFFICIENT_FUNDS_EXCEPTION);
    }

}
