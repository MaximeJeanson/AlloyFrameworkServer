package org.alloyframework.server.impl.server;

import org.alloyframework.server.impl.presenter.alloy.AlloyPresenterImpl;
import org.alloyframework.presenter.Presenter;
import org.alloyframework.server.Application;

import java.io.IOException;
import java.net.InetAddress;

public class ApplicationImpl implements Application {
    private Presenter[] presenters;

    @Override
    public Presenter[] getPresenters() {
        return presenters;
    }

    @Override
    public void bootstrap(final String[] args) {
        try {
            presenters = new Presenter[] { new AlloyPresenterImpl(null, 100, InetAddress.getLocalHost()) };
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
