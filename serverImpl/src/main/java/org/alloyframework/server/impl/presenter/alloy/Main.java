package org.alloyframework.server.impl.presenter.alloy;

import com.google.common.reflect.Reflection;
import org.alloyframework.banking.contract.boundary.Banking;
import org.alloyframework.banking.contract.entitygateway.BankingAccountEntityGateway;
import org.alloyframework.banking.contract.model.BankingAccount;
import org.alloyframework.presenter.Presenter;
import org.alloyframework.server.Application;
import org.alloyframework.server.contract.SecurityProvider;
import org.alloyframework.server.contract.annotation.BoundaryMethod;
import org.alloyframework.server.contract.annotation.EntityGatewayImplementation;
import org.alloyframework.server.contract.boundary.Boundaries;
import org.alloyframework.server.contract.boundary.Boundary;
import org.alloyframework.server.contract.boundary.BoundaryContext;
import org.alloyframework.server.contract.boundary.RequestModel;
import org.alloyframework.server.contract.boundary.ResponseModel;
import org.alloyframework.server.contract.entitygateway.EntityGateway;
import org.alloyframework.server.model.Model;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import javax.annotation.Nonnull;
import javax.management.InstanceNotFoundException;
import javax.security.auth.login.AccountNotFoundException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class Main {
    private Main() {
    }

    public static void main(String[] args) {
        final Application application = new PrototypeApplication();
        final BoundaryContext context = (BoundaryContext) application;

        application.bootstrap(args);

        try {
            final BankingAccountEntityGateway bankingAccountEntityGateway
                    = context.getEntityGateway(
                    BankingAccount.class, BankingAccountEntityGateway.class);

            System.out.println(bankingAccountEntityGateway.find(1l).getFunds());

            Boundaries.invoke(Banking.Withdraw.class,
                    context, Banking.AccountTransactionRequest.builder()
                            .accountID(1)
                            .amount(150.14)
                            .build());

            System.out.println(bankingAccountEntityGateway.find(1l).getFunds());

            Boundaries.invoke(Banking.Deposit.class,
                    context, Banking.AccountTransactionRequest.builder()
                            .accountID(1)
                            .amount(150.14)
                            .build());
            System.out.println(bankingAccountEntityGateway.find(1l).getFunds());

            Boundaries.invoke(Banking.Transfer.class,
                    context, Banking.TransferRequest.builder()
                            .sourceAccountID(1)
                            .targetAccountID(3)
                            .amount(127.14)
                            .build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    final static class PrototypeApplication
            implements Application, BoundaryContext {
        private final BoundaryContext boundaryContext;

        private final Principal principal;

        private final SecurityProvider securityProvider;

        private final Map<Class, EntityGateway> entityGatewayRegistry;

        private final Map<Class, Boundary> boundaryRegistry;

        public PrototypeApplication() {
            this.entityGatewayRegistry = new HashMap<>();
            this.boundaryRegistry = new HashMap<>();
            this.principal = new User();
            this.securityProvider = new BlackHoleSecurityProvider();
            this.boundaryContext = new BoundaryContextImpl(
                    entityGatewayRegistry, boundaryRegistry, securityProvider,
                    principal);
        }

        @Override
        public Presenter[] getPresenters() {
            return new Presenter[0];
        }

        @Override
        public void bootstrap(final String[] args) {
            final ConfigurationBuilder configurationBuilder
                    = new ConfigurationBuilder()
                    .setUrls(ClasspathHelper.forPackage("org.alloyframework"))
                    .setScanners(new MethodAnnotationsScanner(), new TypeAnnotationsScanner(), new
                            SubTypesScanner());

            final Reflections reflections
                    = new Reflections(configurationBuilder);

            final Set<Class<?>> entityGatewayImplementations
                    = reflections.getTypesAnnotatedWith(
                    EntityGatewayImplementation.class);

            for (Class<?> entityGatewayImplementation : entityGatewayImplementations) {
                final EntityGatewayImplementation annotation = entityGatewayImplementation
                        .getAnnotation(EntityGatewayImplementation.class);

                try {
                    entityGatewayRegistry.putIfAbsent(annotation.value(), (EntityGateway)
                            entityGatewayImplementation.newInstance());
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

            final Set<Method> boundaryMethods
                    = reflections.getMethodsAnnotatedWith(BoundaryMethod.class);

            for (final Method boundaryMethod : boundaryMethods) {
                final BoundaryMethod annotation
                        = boundaryMethod.getAnnotation(BoundaryMethod.class);

                boundaryMethod.setAccessible(true);

                final Class<? extends Boundary> boundaryType = annotation.value();

                final InvocationHandler invocationHandler
                        = new InvocationHandler() {
                    @Override
                    public Object invoke(
                            final Object proxy,
                            final Method method,
                            final Object[] args1) throws Throwable {
                        try {
                            return boundaryMethod.invoke(null, args1);
                        } catch (IllegalAccessException | IllegalArgumentException e) {
                            throw e;
                        } catch (InvocationTargetException e) {
                            throw e.getCause();
                        }
                    }
                };

                final Boundary boundary
                        = Reflection.newProxy(boundaryType, invocationHandler);

                boundaryRegistry.putIfAbsent(boundaryType, boundary);
            }

        }

        @Override
        public Map<Class, EntityGateway> getEntityGatewayRegistry() {
            return boundaryContext.getEntityGatewayRegistry();
        }

        @Override
        public Map<Class, Boundary> getBoundaryRegistry() {
            return boundaryContext.getBoundaryRegistry();
        }

        @Override
        @Nonnull
        public <ModelType extends Model, EntityGatewayType extends EntityGateway<ModelType>>
        EntityGatewayType getEntityGateway(
                @Nonnull final Class<ModelType> modelType,
                @Nonnull final Class<EntityGatewayType> entityGatewayType) throws InstanceNotFoundException {
            return boundaryContext.getEntityGateway(modelType, entityGatewayType);
        }

        @Override
        @Nonnull
        public <ModelType extends Model> EntityGateway<ModelType> getEntityGateway(@Nonnull final
                                                                                           Class<ModelType> modelType) throws InstanceNotFoundException {
            return boundaryContext.getEntityGateway(modelType);
        }

        @Override
        @Nonnull
        public <RequestModelType extends RequestModel, ResponseModelType extends ResponseModel,
                BoundaryType extends Boundary<RequestModelType, ResponseModelType>> BoundaryType
        getBoundary(
                @Nonnull final Class<BoundaryType> boundaryType) throws InstanceNotFoundException {
            return boundaryContext.getBoundary(boundaryType);
        }

        @Override
        public SecurityProvider getSecurityProvider() {
            return boundaryContext.getSecurityProvider();
        }

        @Override
        public Principal getCurrentUser() {
            return boundaryContext.getCurrentUser();
        }
    }

    private static final class User implements Principal {

        private String name;

        User() {
            this.name = UUID.randomUUID().toString();
        }

        @Override
        public String getName() {
            return name;
        }
    }

    private static final class BlackHoleSecurityProvider implements SecurityProvider {
        @Override
        public boolean hasAccessTo(
                final AccessType accessType,
                final String accessObject) {
            return true;
        }
    }

    private static final class BoundaryContextImpl implements BoundaryContext {
        private final Map<Class, EntityGateway> entityGatewayRegistry;

        private final Map<Class, Boundary> boundaryRegistry;

        private final SecurityProvider securityProvider;

        private final Principal principal;

        private BoundaryContextImpl(
                final Map<Class, EntityGateway> entityGatewayRegistry,
                final Map<Class, Boundary> boundaryRegistry,
                final SecurityProvider securityProvider,
                final Principal principal) {
            this.entityGatewayRegistry = entityGatewayRegistry;
            this.boundaryRegistry = boundaryRegistry;
            this.securityProvider = securityProvider;
            this.principal = principal;
        }

        @Override
        public Map<Class, EntityGateway> getEntityGatewayRegistry() {
            return entityGatewayRegistry;
        }

        @Override
        public Map<Class, Boundary> getBoundaryRegistry() {
            return boundaryRegistry;
        }

        @Nonnull
        @Override
        public <ModelType extends Model>
        EntityGateway<ModelType> getEntityGateway(
                @Nonnull final Class<ModelType> modelType)
                throws InstanceNotFoundException {
            if (!entityGatewayRegistry.containsKey(modelType))
                throw new InstanceNotFoundException();

            return entityGatewayRegistry.get(modelType);
        }

        @Nonnull
        @Override
        public <RequestModelType extends RequestModel,
                ResponseModelType extends ResponseModel,
                BoundaryType extends Boundary<RequestModelType, ResponseModelType>>
        BoundaryType getBoundary(
                @Nonnull final Class<BoundaryType> boundaryType)
                throws InstanceNotFoundException {
            if (!boundaryRegistry.containsKey(boundaryType))
                throw new InstanceNotFoundException();

            return boundaryType.cast(boundaryRegistry.get(boundaryType));
        }

        @Override
        public SecurityProvider getSecurityProvider() {
            return securityProvider;
        }

        @Override
        public Principal getCurrentUser() {
            return principal;
        }
    }
}
