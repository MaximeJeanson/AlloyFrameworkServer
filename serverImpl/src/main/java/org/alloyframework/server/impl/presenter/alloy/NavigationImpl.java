package org.alloyframework.server.impl.presenter.alloy;

import org.alloyframework.presenter.alloy.Navigation;
import org.alloyframework.presenter.alloy.ViewController;

import javax.annotation.Nonnull;

public class NavigationImpl implements Navigation {
    @Override
    public <ViewControllerType extends ViewController> void open(
            @Nonnull final Class<ViewControllerType> viewControllerType) {

    }

    @Override
    public void back(final int step) {

    }

    @Override
    public void back(@Nonnull final Entry entry) {

    }

    @Override
    public void back() {

    }

    @Override
    public void home() {

    }
}
