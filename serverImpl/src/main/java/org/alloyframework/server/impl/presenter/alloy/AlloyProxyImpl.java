package org.alloyframework.server.impl.presenter.alloy;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.util.CharsetUtil;
import org.alloyframework.presenter.alloy.ClientContext;
import org.alloyframework.presenter.alloy.Component;
import org.alloyframework.presenter.alloy.Proxy;

import static io.netty.handler.codec.http.HttpMethod.GET;
import static io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST;
import static io.netty.handler.codec.http.HttpResponseStatus.FORBIDDEN;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

public class AlloyProxyImpl implements Proxy {
    private final ClientContext context;

    public AlloyProxyImpl(final ClientContext context) {
        this.context = context;

        new Thread(() -> {
            final EventLoopGroup group = new NioEventLoopGroup();

            try {
                final Bootstrap bootstrap = new Bootstrap();

                bootstrap.group(group)
                        .channel(NioServerSocketChannel.class)
                        .option(ChannelOption.TCP_NODELAY, true)
                        .handler(new ChannelInitializer<io.netty.channel.socket.SocketChannel>() {
                            private void sendHttpResponse(ChannelHandlerContext ctx, FullHttpRequest req, FullHttpResponse res) {
                                // Generate an error page if response getStatus code is not OK (200).
                                if (res.status().code() != 200) {
                                    ByteBuf buf = Unpooled.copiedBuffer(res.status().toString(), CharsetUtil.UTF_8);
                                    res.content().writeBytes(buf);
                                    buf.release();
                                    HttpUtil.setContentLength(res, res.content().readableBytes());
                                }

                                // Send the response and close the connection if necessary.
                                ChannelFuture f = ctx.channel().writeAndFlush(res);
                                if (!HttpUtil.isKeepAlive(req) || (res.status().code() != 200)) {
                                    f.addListener(ChannelFutureListener.CLOSE);
                                }
                            }

                            @Override
                            protected void initChannel(final io.netty.channel.socket
                                                                .SocketChannel socketChannel) throws Exception {
                                final ChannelPipeline pipeline = socketChannel.pipeline();

                                pipeline.addLast(new HttpServerCodec());
                                pipeline.addLast(new HttpObjectAggregator(65536));
                                pipeline.addLast(new WebSocketServerProtocolHandler("/", null, true));
                                pipeline.addLast(new SimpleChannelInboundHandler<FullHttpRequest>() {
                                    @Override
                                    protected void channelRead0(
                                            final ChannelHandlerContext ctx,
                                            final FullHttpRequest req) throws Exception {
                                        // Handle a bad request.
                                        if (!req.decoderResult().isSuccess()) {
                                            sendHttpResponse(ctx, req, new DefaultFullHttpResponse(HTTP_1_1, BAD_REQUEST));
                                            return;
                                        }

                                        // Allow only GET methods.
                                        if (req.method() != GET) {
                                            sendHttpResponse(ctx, req, new DefaultFullHttpResponse(HTTP_1_1, FORBIDDEN));
                                            return;
                                        }

                                        receive(req);
                                    }
                                });

                                pipeline.addLast(new WebSocketServerProtocolHandler("/"));
                            }
                        });

                    bootstrap.bind().sync().channel().closeFuture().sync();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public <ComponentType extends Component> void registerComponent(
            final ComponentType component) {

    }

    @Override
    public void receive(final FullHttpRequest request) {

    }

    @Override
    public void send(final Object data) {

    }
}
