package org.alloyframework.server.impl.entitygateway;

import org.alloyframework.banking.contract.model.BankingAccount;
import org.alloyframework.banking.contract.entitygateway.BankingAccountEntityGateway;
import org.alloyframework.server.contract.annotation.EntityGatewayImplementation;
import org.alloyframework.server.model.Model;

import javax.annotation.Nonnull;
import javax.security.auth.login.AccountNotFoundException;
import java.util.Arrays;
import java.util.Optional;

@EntityGatewayImplementation(BankingAccount.class)
public class BankAccountEntityGatewayImpl implements BankingAccountEntityGateway {
    private final BankAccountEntity[] entities;

    public BankAccountEntityGatewayImpl() {
        this.entities = new BankAccountEntity[100];

        for (int i = 0; i < 100; i++) {
            entities[i] = new BankAccountEntity();
            entities[i].id = i;
            entities[i].funds = Math.random() * 10.0;
        }
    }

    @Nonnull
    @Override
    public Class<BankingAccount> getModelType() {
        return BankingAccount.class;
    }

    @Override
    public BankingAccount find(
            final Long identity)
            throws Exception {
        final BankAccountEntity bankAccountEntity1
                = Arrays.stream(entities)
                .filter(bankAccountEntity -> bankAccountEntity.id == identity)
                .findFirst()
                .orElseThrow(AccountNotFoundException::new);

        return new BankingAccount() {
            @Override
            public long getID() {
                return bankAccountEntity1.id;
            }

            @Override
            public void setID(final long id) {
                bankAccountEntity1.id = id;
            }

            @Override
            public double getFunds() {
                return bankAccountEntity1.funds;
            }

            @Override
            public double getLineOfCredit() {
                return 1200;
            }

            @Override
            public void setFunds(final double amount) {
                bankAccountEntity1.funds = amount;
            }

            @Override
            public void setLineOfCredit(final double value) {

            }

            @Override
            public boolean isFrozen() {
                return false;
            }

            @Override
            public void setFrozen(final boolean value) {

            }

            @Override
            public boolean hasLineOfCredit() {
                return true;
            }

            @Override
            public void close() throws Exception {

            }

            @Override
            public int compareTo(final Model o) {
                return 0;
            }
        };
    }
}
