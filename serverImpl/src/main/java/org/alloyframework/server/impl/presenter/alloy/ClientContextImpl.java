package org.alloyframework.server.impl.presenter.alloy;

import org.alloyframework.presenter.alloy.ClientContext;
import org.alloyframework.presenter.alloy.Navigation;

import javax.annotation.Nonnull;

public class ClientContextImpl implements ClientContext {
    private final Navigation navigation;

    public ClientContextImpl() {
        navigation = new NavigationImpl();
    }

    @Nonnull
    @Override
    public Navigation getNavigation() {
        return navigation;
    }
}
