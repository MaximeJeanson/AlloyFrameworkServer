package org.alloyframework.server.impl.presenter.alloy;

import org.alloyframework.presenter.Presenter;
import org.alloyframework.presenter.alloy.ClientContext;

import javax.net.ServerSocketFactory;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

public class AlloyPresenterImpl implements Presenter {
    private final ServerSocket serverSocket;
    private final List<Socket> clients;

    public AlloyPresenterImpl(
            final ServerSocketFactory serverSocketFactory,
            final int port,
            final InetAddress address)
            throws IOException {
        this.clients = new LinkedList<>();
        this.serverSocket = serverSocketFactory.createServerSocket();

        new Thread(() -> {
            while (!serverSocket.isClosed()) {
                try {
                    final Socket socket = serverSocket.accept();

                    clients.add(socket);

                    ClientContext context = new ClientContextImpl();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
