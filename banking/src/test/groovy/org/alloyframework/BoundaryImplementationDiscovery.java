package org.alloyframework;

import com.google.common.reflect.Reflection;
import org.alloyframework.server.contract.SecurityProvider;
import org.alloyframework.server.contract.annotation.BoundaryMethod;
import org.alloyframework.server.contract.annotation.EntityGatewayDefinition;
import org.alloyframework.server.contract.boundary.Boundary;
import org.alloyframework.server.contract.boundary.BoundaryContext;
import org.alloyframework.server.contract.boundary.RequestModel;
import org.alloyframework.server.contract.boundary.ResponseModel;
import org.alloyframework.server.contract.entitygateway.EntityGateway;
import org.alloyframework.server.model.Model;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import javax.annotation.Nonnull;
import javax.management.InstanceNotFoundException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.text.WordUtils.uncapitalize;

public class BoundaryImplementationDiscovery {
    public static final SecurityProvider allowsAllSP
            = new SecurityProvider() {
        @Override
        public boolean hasAccessTo(
                final AccessType accessType,
                final String accessObject) {
            return true;
        }
    };

    private static final Map<Class, Boundary> BOUNDARY_IMPLEMENTATION_REGISTRY
            = getBoundaryImplementations();

    private static final Map<Class<Model>, Class<EntityGateway<Model>>> ENTITY_GATEWAY_IMPLEMENTATION_REGISTRY
            = getEntityGatewayImplementations();

    private static final Pattern METHOD_ACTION_PATTERN
            = Pattern.compile("^(get|is|set)(.*?)$");

    private BoundaryImplementationDiscovery() {
    }

    public static Map<Class, Boundary> getBoundaryImplementations() {
        if (Objects.nonNull(BOUNDARY_IMPLEMENTATION_REGISTRY))
            return BOUNDARY_IMPLEMENTATION_REGISTRY;

        final Map<Class, Boundary> boundaryRegistry
                = new HashMap<>();

        final ConfigurationBuilder configurationBuilder
                = new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage("org.alloyframework"))
                .setScanners(
                        new MethodAnnotationsScanner(),
                        new TypeAnnotationsScanner(),
                        new SubTypesScanner());

        final Reflections reflections
                = new Reflections(configurationBuilder);

        final Set<Method> boundaryMethods
                = reflections.getMethodsAnnotatedWith(BoundaryMethod.class);

        for (final Method boundaryMethod : boundaryMethods) {
            final BoundaryMethod annotation
                    = boundaryMethod.getAnnotation(BoundaryMethod.class);

            boundaryMethod.setAccessible(true);

            final Class<? extends Boundary> boundaryType = annotation.value();

            final InvocationHandler invocationHandler
                    = new InvocationHandler() {
                @Override
                public Object invoke(
                        final Object proxy,
                        final Method method,
                        final Object[] args1) throws Throwable {
                    if (method.getName().equalsIgnoreCase("execute")) try {
                        return boundaryMethod.invoke(null, args1);
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        throw e;
                    } catch (InvocationTargetException e) {
                        throw e.getCause();
                    }

                    return method.invoke(boundaryMethod, args1);
                }
            };

            final Boundary boundary
                    = Reflection.newProxy(boundaryType, invocationHandler);

            boundaryRegistry.putIfAbsent(boundaryType, boundary);
        }

        return boundaryRegistry;
    }

    public static Map<Class<Model>, Class<EntityGateway<Model>>> getEntityGatewayImplementations() {
        if (Objects.nonNull(ENTITY_GATEWAY_IMPLEMENTATION_REGISTRY))
            return ENTITY_GATEWAY_IMPLEMENTATION_REGISTRY;

        final Map<Class<Model>, Class<EntityGateway<Model>>> entityGatewayRegistry
                = new HashMap<>();

        final ConfigurationBuilder configurationBuilder
                = new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage("org.alloyframework"))
                .setScanners(
                        new MethodAnnotationsScanner(),
                        new TypeAnnotationsScanner(),
                        new SubTypesScanner());

        final Reflections reflections
                = new Reflections(configurationBuilder);

        final Set<Class<?>> entityGatewayTypes
                = reflections.getTypesAnnotatedWith(EntityGatewayDefinition.class);

        for (final Class<?> entityGatewayType : entityGatewayTypes) {
            final EntityGatewayDefinition annotation
                    = entityGatewayType.getAnnotation(EntityGatewayDefinition.class);

            final Class<? extends Model> modelType = annotation.value();

            entityGatewayRegistry.putIfAbsent((Class<Model>) modelType, (Class<EntityGateway<Model>>) entityGatewayType);
        }

        return entityGatewayRegistry;
    }

    public static <ModelType extends Model>
    ModelType createModel(final Class<ModelType> modelType) {
        final Map<String, Object> storage = new HashMap<>();

        return Reflection.newProxy(modelType, (proxy, method, args) -> {
            final String methodName = method.getName();

            final Matcher matcher = METHOD_ACTION_PATTERN.matcher(methodName);

            if (matcher.find()) {
                final String action = matcher.group(1);
                final String propertyName = uncapitalize(matcher.group(2));

                if (action.equalsIgnoreCase("get") || action.equalsIgnoreCase("is"))
                    return storage.getOrDefault(propertyName, null);

                if (action.equalsIgnoreCase("set"))
                    storage.put(propertyName, args[0]);
            }

            if (methodName.equalsIgnoreCase("toString"))
                return storage.toString();

            if (methodName.equalsIgnoreCase("hashCode"))
                return storage.hashCode();

            return null;
        });
    }

    public static <
            ModelType extends Model,
            EntityGatewayType extends EntityGateway<ModelType>>
    EntityGatewayType createEntityGatewayMock(
            final Class<EntityGatewayType> entityGatewayType,
            final Class<ModelType> modelType,
            final Collection<ModelType> models) {
        return Reflection.newProxy(entityGatewayType, (proxy, method, args) -> {
            final Collection<ModelType> _models = models;

//            System.out.println(models);

            if (method.getReturnType().isAssignableFrom(Collection.class))
                return models;

            if (method.getReturnType().isAssignableFrom(modelType))
                return models.stream().findFirst()
                        .orElseThrow(UnsupportedOperationException::new);

            throw new UnsupportedOperationException(method.toGenericString());
        });
    }

    public static final class User implements Principal {

        private String name;

        User() {
            this.name = UUID.randomUUID().toString();
        }

        @Override
        public String getName() {
            return name;
        }
    }

    public static final class BoundaryContextImpl implements BoundaryContext {
        private final Map<Class, EntityGateway> entityGatewayRegistry;

        private final Map<Class, Boundary> boundaryRegistry;

        private final SecurityProvider securityProvider;

        private final Principal principal;

        public BoundaryContextImpl(
                final Map<Class, EntityGateway> entityGatewayRegistry,
                final Map<Class, Boundary> boundaryRegistry,
                final SecurityProvider securityProvider,
                final Principal principal) {
            this.entityGatewayRegistry = entityGatewayRegistry;
            this.boundaryRegistry = boundaryRegistry;
            this.securityProvider = securityProvider;
            this.principal = principal;
        }

        @Override
        public Map<Class, EntityGateway> getEntityGatewayRegistry() {
            return entityGatewayRegistry;
        }

        @Override
        public Map<Class, Boundary> getBoundaryRegistry() {
            return boundaryRegistry;
        }

        @Nonnull
        @Override
        public <ModelType extends Model>
        EntityGateway<ModelType> getEntityGateway(
                @Nonnull final Class<ModelType> modelType)
                throws InstanceNotFoundException {
            if (!entityGatewayRegistry.containsKey(modelType))
                throw new InstanceNotFoundException();

            return entityGatewayRegistry.get(modelType);
        }

        @Nonnull
        @Override
        public <RequestModelType extends RequestModel,
                ResponseModelType extends ResponseModel,
                BoundaryType extends Boundary<RequestModelType, ResponseModelType>>
        BoundaryType getBoundary(
                @Nonnull final Class<BoundaryType> boundaryType)
                throws InstanceNotFoundException {
            if (!boundaryRegistry.containsKey(boundaryType))
                throw new InstanceNotFoundException();

            return boundaryType.cast(boundaryRegistry.get(boundaryType));
        }

        @Override
        public SecurityProvider getSecurityProvider() {
            return securityProvider;
        }

        @Override
        public Principal getCurrentUser() {
            return principal;
        }
    }
}
