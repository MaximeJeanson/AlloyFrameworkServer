package org.alloyframework;

import org.alloyframework.server.contract.boundary.BoundaryContext;
import org.alloyframework.server.model.Model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class BoundaryScenario {
    public final Collection<Model> models;

    public BoundaryContext context;

    public Exception exception;

    public Object result;

    public BoundaryScenario() {
        this.models = new ArrayList<>();
    }

    public static Object parse(final String value, final Class type) {
        if (value.equalsIgnoreCase("null"))
            return null;

        if (double.class.isAssignableFrom(type))
            return Double.parseDouble(value);

        if (int.class.isAssignableFrom(type))
            return Integer.parseInt(value);

        if (boolean.class.isAssignableFrom(type))
            return Boolean.parseBoolean(value);

        if (long.class.isAssignableFrom(type))
            return Long.parseLong(value);

        throw new AssertionError();
    }

    public static Object parse(final String value, final Object typeInstance) {
        if (Objects.nonNull(typeInstance))
            return parse(value, typeInstance.getClass());

        return parse(value, (Class) null);
    }
}
