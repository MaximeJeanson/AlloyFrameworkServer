package org.alloyframework.banking.boundary.accounts;

import cucumber.api.java.en.When;
import cucumber.api.java8.En;
import org.alloyframework.BoundaryScenario;
import org.alloyframework.banking.contract.boundary.Banking;
import org.alloyframework.server.contract.boundary.BoundaryContext;

import javax.management.InstanceNotFoundException;

import static org.alloyframework.BoundaryImplementationDiscovery.getBoundaryImplementations;

public class Deposit
        implements En {

    private final BoundaryScenario scenario;

    public Deposit(final BoundaryScenario scenario) {
        this.scenario = scenario;
    }

    @When("^Deposits (\\d+) to (\\d+)$")
    public void whenDeposit(final int amount, final int accountID) {
        final Banking.AccountTransactionRequest request
                = Banking.AccountTransactionRequest.builder()
                .accountID(accountID)
                .amount(amount)
                .build();

        final BoundaryContext context = scenario.context;

        context.getBoundaryRegistry().putIfAbsent(
                Banking.Deposit.class, getBoundaryImplementations().get(Banking.Deposit.class));


        try {
            final Banking.Deposit boundary
                    = context.getBoundary(Banking.Deposit.class);
            try {
                scenario.result = boundary.execute(context, request);
            } catch (Exception e) {
                scenario.exception = e;
            }
        } catch (InstanceNotFoundException e) {
            throw new AssertionError(e);
        }

    }
}
