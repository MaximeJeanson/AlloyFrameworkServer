package org.alloyframework.banking.boundary.accounts;

import cucumber.api.java.en.When;
import cucumber.api.java8.En;
import org.alloyframework.BoundaryScenario;
import org.alloyframework.banking.contract.boundary.Banking;
import org.alloyframework.server.contract.boundary.Boundaries;

import static org.alloyframework.BoundaryImplementationDiscovery.getBoundaryImplementations;

public class Withdraw
        implements En {

    private final BoundaryScenario scenario;

    public Withdraw(final BoundaryScenario scenario) {
        this.scenario = scenario;
    }

    @When("^Withdraws (\\d+) from (\\d+)$")
    public void withdraws(
            final int amount,
            final int accountID) {
        final Banking.AccountTransactionRequest request
                = Banking.AccountTransactionRequest.builder()
                .accountID(accountID)
                .amount(amount)
                .build();

        scenario.context.getBoundaryRegistry().putIfAbsent(
                Banking.Withdraw.class, getBoundaryImplementations().get(Banking.Withdraw.class));

        try {
            scenario.result = Boundaries.invoke(
                    Banking.Withdraw.class, scenario.context, request);
        } catch (Exception e) {
            scenario.exception = e;
        }
    }
}
