package org.alloyframework;

import cucumber.api.CucumberOptions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.alloyframework.BoundaryImplementationDiscovery.BoundaryContextImpl;
import org.alloyframework.server.contract.entitygateway.EntityGateway;
import org.alloyframework.server.model.Model;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.reflections.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Objects;
import java.util.function.Function;

import static org.alloyframework.BoundaryImplementationDiscovery.*;
import static org.alloyframework.BoundaryImplementationDiscovery.allowsAllSP;
import static org.alloyframework.BoundaryImplementationDiscovery.createEntityGatewayMock;
import static org.alloyframework.BoundaryImplementationDiscovery.createModel;
import static org.alloyframework.BoundaryImplementationDiscovery.getEntityGatewayImplementations;
import static org.alloyframework.BoundaryScenario.parse;

public class BoundarySteps {

    private final BoundaryScenario scenario;

    public BoundarySteps(final BoundaryScenario scenario) {
        this.scenario = scenario;
    }

    @Given("^An instance of (.*?) (\\d+)$")
    public void givenInstanceOf(
            final String modelFQName,
            final int id) {
        scenario.context = new BoundaryContextImpl(
                new HashMap<>(), new HashMap<>(), allowsAllSP, new User());

        try {
            final Class modelType = Class.forName(modelFQName);

            final Class entityGatewayType
                    = getEntityGatewayImplementations().get(modelType);

            final EntityGateway entityGatewayMock
                    = createEntityGatewayMock((Class<EntityGateway>) entityGatewayType, (Class<Model>) modelType, scenario.models);

            scenario.context.getEntityGatewayRegistry()
                    .put(modelType, entityGatewayMock);

            Model model = createModel(modelType);
            model.setID(id);
            scenario.models.add(model);
        } catch (ClassNotFoundException e) {
            throw new AssertionError(e);
        }
    }

    @And("^where (\\d+)\\.(.*?) = (.*?)$")
    public void andWhere(
            final long id,
            final String field,
            final String value) {
        final Model model = scenario.models.stream()
                .filter(o -> o.getID() == id)
                .findFirst()
                .orElseThrow(UnsupportedOperationException::new);

        final Method[] methods = model.getClass().getMethods();

        for (Method method : methods) {
            if (method.getName().equalsIgnoreCase("set" + field)) {
                try {
                    final Class valueType = method.getParameterTypes()[0];
                    final Object v = parse(value, valueType);

                    method.invoke(model, v);
                    return;
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new AssertionError(e);
                }
            }
        }
    }

    @Then("^throws (.*?)$")
    public void thenThrows(final String exceptionFQName) {
        final Exception exception = scenario.exception;

        if (exceptionFQName.equalsIgnoreCase("NOTHING")) {
            MatcherAssert.assertThat(exception, CoreMatchers.nullValue());
        } else {
            try {
                final Class type = Class.forName(exceptionFQName);

                if (Objects.isNull(exception))
                    System.out.println("");

                MatcherAssert.assertThat(exception, CoreMatchers.instanceOf(type));
            } catch (ClassNotFoundException e) {
                throw new AssertionError(e);
            }
        }
    }

    @Then("^returns ([^\\s]*?)$")
    public void thenReturnsValue(final String value) {
        final Object result = scenario.result;

        MatcherAssert.assertThat(result, CoreMatchers.is(parse(value, result)));
    }

    @Then("^returns (.*?) (.*?) (.*?)$")
    public void thenReturnsOperator(
            final String property,
            final String operator,
            final String value) {
        final Function<Object, Matcher<Object>> matcher;

        if (operator.equalsIgnoreCase("=="))
            matcher = CoreMatchers::is;
        else
            throw new UnsupportedOperationException(operator);

        try {
            final Object result = scenario.result;

            final Field field = ReflectionUtils.getAllFields(result.getClass(), input -> input
                    .getName().equalsIgnoreCase(property)).stream().findFirst().orElseThrow
                    (NoSuchFieldError::new);
            field.setAccessible(true);

            final Class type = field.getType();

            MatcherAssert.assertThat(field.get(result), matcher.apply(parse(value, type)));
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }

}
