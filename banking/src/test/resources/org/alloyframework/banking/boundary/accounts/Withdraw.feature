Business Need: Withdraws funds from a bank account

  Scenario Outline: Withdraw
    Given An instance of org.alloyframework.banking.contract.model.BankingAccount <accountID>
    And where <accountID>.funds = <funds>
    And where <accountID>.lineOfCredit = <lineOfCredit>
    And where <accountID>.frozen = <frozen>
    When Withdraws <amount> from <accountID>
    Then throws <exception>
    Then returns <result>

    Examples:
      | accountID | funds | lineOfCredit | frozen | amount | exception                                                                       | result        |
      | 1         | 0     | 0            | false  | 1      | org.alloyframework.banking.contract.boundary.Banking$InsufficientFundsException | null          |
      | 2         | 0     | 0            | false  | 0      | java.lang.IllegalArgumentException                                              | null          |
      | 3         | 0     | 0            | true   | 1      | org.alloyframework.banking.contract.boundary.Banking$FrozenAccountException     | null          |
      | 4         | 100   | 0            | false  | 1      | NOTHING                                                                         | amount == 1.0 |
      | 5         | 0     | 100          | false  | 1      | NOTHING                                                                         | amount == 1.0 |