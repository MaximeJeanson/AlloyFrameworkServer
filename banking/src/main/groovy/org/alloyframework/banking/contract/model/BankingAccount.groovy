package org.alloyframework.banking.contract.model;

import org.alloyframework.server.model.Model;

interface BankingAccount extends Model {
    double getFunds();

    double getLineOfCredit();

    void setFunds(final double value);

    void setLineOfCredit(final double value);

    boolean isFrozen();

    void setFrozen(boolean value);

    boolean hasLineOfCredit();
}
