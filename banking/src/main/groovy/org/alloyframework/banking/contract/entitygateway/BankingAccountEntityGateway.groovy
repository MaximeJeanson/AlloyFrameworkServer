package org.alloyframework.banking.contract.entitygateway;

import org.alloyframework.banking.contract.model.BankingAccount
import org.alloyframework.server.contract.annotation.EntityGatewayDefinition;
import org.alloyframework.server.contract.entitygateway.EntityGateway;
import org.alloyframework.server.contract.entitygateway.action.FindByIdAction;

@EntityGatewayDefinition(BankingAccount)
public interface BankingAccountEntityGateway
        extends EntityGateway<BankingAccount>,
                FindByIdAction<BankingAccount, Long> {
}
