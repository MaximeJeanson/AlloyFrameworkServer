package org.alloyframework.banking.contract.boundary;

import groovy.transform.InheritConstructors;
import groovy.transform.builder.Builder;
import org.alloyframework.server.contract.boundary.Boundary;
import org.alloyframework.server.contract.boundary.RequestModel;
import org.alloyframework.server.contract.boundary.ResponseModel;

public final class Banking {
    private Banking() {
    }

    @InheritConstructors
    static class InsufficientFundsException extends Exception {}

    @InheritConstructors
    static class FrozenAccountException extends Exception {}

    public static final String NEGATIVE_OR_ZERO_AMOUNT_TRANSFER_EXCEPTION =
            "Can't transfer negative or zero funds between accounts";

    public static final String FROZEN_ACCOUNT_EXCEPTION =
            "Can't transfer funds from or to a frozen account";

    public static final String INSUFFICIENT_FUNDS_EXCEPTION =
            "Insufficient funds for transaction";

    /**
     * Withdraws an amount of funds from a banking account.
     *
     * @throws IllegalArgumentException
     *      when the amount is negative or zero.
     *      when the accountID is invalid.
     *
     * @throws InsufficientFundsException
     *      when the funds plus the line of credit associated with the given
     *      account is smaller than the requested amount.
     *
     * @throws FrozenAccountException
     *      when the given account is frozen and therefor transactions are not
     *      allowed.
     *
     * @returns the amount of withdrawn funds from the given account.
     *
     * @see org.alloyframework.banking.contract.entitygateway.BankingAccountEntityGateway
     */
    interface Withdraw
            extends Boundary<AccountTransactionRequest, AccountTransactionResponse> {
    }

    /**
     * Deposits an amount of funds to a banking account.
     *
     * @throws IllegalArgumentException
     *      when the amount is negative or zero.
     *      when the accountID is invalid.
     *
     * @throws FrozenAccountException
     *      when the given account is frozen and therefor transactions are not
     *      allowed.
     *
     * @returns the amount of deposited funds to the given account.
     *
     * @see org.alloyframework.banking.contract.entitygateway.BankingAccountEntityGateway
     */
    interface Deposit
            extends Boundary<AccountTransactionRequest, AccountTransactionResponse> {
    }

    /**
     * Transfers an amount of funds from a banking account to another.
     *
     * @throws IllegalArgumentException
     *      when the amount is negative or zero.
     *      when an accountID is invalid.
     *
     * @returns the amount of transferred funds between accounts.
     *
     * @see org.alloyframework.banking.contract.entitygateway.BankingAccountEntityGateway
     * @see Withdraw
     * @see Deposit
     */
    interface Transfer
            extends Boundary<TransferRequest, TransferResponse> {}

    @Builder(prefix = "")
    static class TransferRequest implements RequestModel {
        long sourceAccountID;
        long targetAccountID;
        double amount;
    }

    @Builder(prefix = "")
    static class TransferResponse implements ResponseModel {
        long sourceAccountID;
        long targetAccountID;
        double amount;
    }

    @Builder(prefix = "")
    static class AccountTransactionRequest implements RequestModel {
        long accountID;

        double amount;
    }

    @Builder(prefix = "")
    static class AccountTransactionResponse implements ResponseModel {
        long accountID;

        double amount;
    }

}
