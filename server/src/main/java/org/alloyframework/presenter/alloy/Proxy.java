package org.alloyframework.presenter.alloy;

import io.netty.handler.codec.http.FullHttpRequest;

public interface Proxy {
    <ComponentType extends Component>
    void registerComponent(
            final ComponentType component);

    void receive(final FullHttpRequest data);

    void send(final Object data);
}
