package org.alloyframework.presenter.alloy;

import java.util.Stack;

public interface ViewController {
    ClientContext getContext();

    Stack<ViewController> getNavigation();


}
