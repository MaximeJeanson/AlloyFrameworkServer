package org.alloyframework.presenter.alloy;

import javax.annotation.Nonnull;

public interface ClientContext {
    @Nonnull
    Navigation getNavigation();
}
