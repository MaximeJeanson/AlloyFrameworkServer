package org.alloyframework.presenter.alloy;

import org.alloyframework.presenter.alloy.ViewController;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

public interface Navigation {
    <ViewControllerType extends ViewController>
    void open(@Nonnull final Class<ViewControllerType> viewControllerType);

    void back(@Nonnegative final int step);

    void back(@Nonnull final Entry entry);

    void back();

    void home();

    interface Entry {

    }
}
