package org.alloyframework.server.contract;

public interface Registry<RegistrableType, KeyType> {
    void register(
            final Class<KeyType> key,
            final RegistrableType registrable);

    RegistrableType get(final Class<KeyType> key);
}
