package org.alloyframework.server.contract;

public interface SecurityProvider {
    enum AccessType {
        BOUNDARY, ENTITY_GATEWAY, ENTITY
    }

    boolean hasAccessTo(final AccessType accessType, final String accessObject);
}
