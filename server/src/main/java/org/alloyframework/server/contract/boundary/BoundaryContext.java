package org.alloyframework.server.contract.boundary;

import org.alloyframework.server.contract.entitygateway.EntityGateway;
import org.alloyframework.server.contract.SecurityProvider;
import org.alloyframework.server.model.Model;

import javax.annotation.Nonnull;
import javax.management.InstanceNotFoundException;
import java.security.Principal;
import java.util.Map;

public interface BoundaryContext {
    Map<Class, EntityGateway> getEntityGatewayRegistry();

    Map<Class, Boundary> getBoundaryRegistry();

    @Nonnull
    default <
            ModelType extends Model,
            EntityGatewayType extends EntityGateway<ModelType>>
    EntityGatewayType getEntityGateway(
            @Nonnull final Class<ModelType> modelType,
            @Nonnull final Class<EntityGatewayType> entityGatewayType)
            throws InstanceNotFoundException{
        return entityGatewayType.cast(getEntityGateway(modelType));
    }

    @Nonnull
    <ModelType extends Model>
    EntityGateway<ModelType> getEntityGateway(
            @Nonnull final Class<ModelType> modelType)
            throws InstanceNotFoundException;

    /**
     * Retrieves the boundary implementation associated with the given {@code boundaryType}
     * registered with the this context.
     *
     * @param boundaryType        The fully-qualified boundary type {@link Class}.
     * @param <RequestModelType>  The request model associated with the boundary.
     * @param <ResponseModelType> The response model associated with the boundary.
     * @param <BoundaryType>      The fully-qualified boundary type.
     * @return The boundary implementation instance.
     */
    @Nonnull
    <RequestModelType extends RequestModel,
            ResponseModelType extends ResponseModel,
            BoundaryType extends Boundary<RequestModelType, ResponseModelType>>
    BoundaryType getBoundary(
            @Nonnull final Class<BoundaryType> boundaryType)
            throws InstanceNotFoundException;

    SecurityProvider getSecurityProvider();

    Principal getCurrentUser();
}
