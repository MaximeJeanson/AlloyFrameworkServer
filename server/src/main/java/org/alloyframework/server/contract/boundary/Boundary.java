package org.alloyframework.server.contract.boundary;

@FunctionalInterface
public interface Boundary<
        RequestModelType extends RequestModel,
        ResponseModelType extends ResponseModel> {
    ResponseModelType execute(
            final BoundaryContext context,
            final RequestModelType requestModelType)
            throws Exception;
}
