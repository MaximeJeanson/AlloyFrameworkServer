package org.alloyframework.server.contract.entitygateway;

import org.alloyframework.server.model.Model;

import javax.annotation.Nonnull;

public interface EntityGatewayAction<
        ModelType extends Model,
        EntityGatewayType extends EntityGateway<ModelType>> {
    @Nonnull
    EntityGatewayType getEntityGateway();
}
