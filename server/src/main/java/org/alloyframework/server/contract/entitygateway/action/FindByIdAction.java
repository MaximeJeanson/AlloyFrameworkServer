package org.alloyframework.server.contract.entitygateway.action;

import org.alloyframework.server.model.Model;

import java.util.Optional;

public interface FindByIdAction<ModelType extends Model, IdentityType> {
    ModelType find(final IdentityType identity) throws Exception;
}
