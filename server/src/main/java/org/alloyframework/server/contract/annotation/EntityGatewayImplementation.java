package org.alloyframework.server.contract.annotation;

import org.alloyframework.server.model.Model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface EntityGatewayImplementation {
    Class<? extends Model> value();
}
