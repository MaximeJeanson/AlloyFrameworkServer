package org.alloyframework.server.contract.entitygateway;

import org.alloyframework.server.model.Model;

import javax.annotation.Nonnull;

public interface EntityGateway<
        ModelType extends Model> {
    @Nonnull
    Class<ModelType> getModelType();
}