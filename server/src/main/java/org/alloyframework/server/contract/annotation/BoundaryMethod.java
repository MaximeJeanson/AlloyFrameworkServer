package org.alloyframework.server.contract.annotation;

import org.alloyframework.server.contract.boundary.Boundary;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface BoundaryMethod {
    Class<? extends Boundary> value();
}
