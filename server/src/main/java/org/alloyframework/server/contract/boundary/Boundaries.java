package org.alloyframework.server.contract.boundary;

public final class Boundaries {
    private Boundaries() {
    }

    public static <
            RequestModelType extends RequestModel,
            ResponseModelType extends ResponseModel,
            BoundaryType extends Boundary<RequestModelType, ResponseModelType>>
    ResponseModelType invoke(
            final Class<BoundaryType> boundaryType,
            final BoundaryContext context,
            final RequestModelType requestModel) throws Exception {
        return context.getBoundary(boundaryType).execute(context, requestModel);
    }
}
