package org.alloyframework.server.model;

public interface Model extends Comparable<Model>, AutoCloseable {
    long getID();

    void setID(long id);
}
