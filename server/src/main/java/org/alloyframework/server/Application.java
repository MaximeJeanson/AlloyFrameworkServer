package org.alloyframework.server;

import org.alloyframework.presenter.Presenter;

public interface Application {
    Presenter[] getPresenters();

    void bootstrap(final String[] args);
}
