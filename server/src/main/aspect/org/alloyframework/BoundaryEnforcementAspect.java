package org.alloyframework;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.DeclareError;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class BoundaryEnforcementAspect {
    @Pointcut(value = "execution(@org.alloyframework.server.contract.annotation.BoundaryMethod !private * *(..)) || " +
            "execution(@org.alloyframework.server.contract.annotation.BoundaryMethod !static * *(..))")
    public void boundaryMethods() {}

    @Pointcut(value = "execution(@org.alloyframework.server.contract.annotation.BoundaryMethod * *(..))")
    public void boundaryMethodExecution() {}

    @Pointcut(value = "call(@org.alloyframework.server.contract.annotation.BoundaryMethod * *(..))")
    public void boundaryMethodCall() {}

    @DeclareError("boundaryMethods()")
    private static final String NON_PRIVATE_NON_STATIC_BOUNDARY_METHOD
            = "Boundary methods are expected to be private static methods.";

    @Before("cflow(boundaryMethodExecution()) && boundaryMethodCall()")
    public void directBoundaryCallError() {
        throw new Error("Boundary methods should never be call directly.  " +
                "Use BoundaryContext#getBoundary method instead.");
    }

//    @Before("execution(* *(..))")
//    public void trace(final JoinPoint joinPoint) {
//        System.out.println(joinPoint.toLongString());
//    }
}
