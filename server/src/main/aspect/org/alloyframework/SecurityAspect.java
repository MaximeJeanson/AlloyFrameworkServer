package org.alloyframework;

import org.alloyframework.server.contract.boundary.BoundaryContext;
import org.alloyframework.server.contract.boundary.RequestModel;
import org.alloyframework.server.contract.SecurityProvider;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class SecurityAspect {
    @Pointcut(value = "execution(@org.alloyframework.server.contract.annotation.BoundaryMethod static * *(..)) && args(context, requestModel)",
              argNames = "context,requestModel")
    public void boundaryMethods(
            final BoundaryContext context,
            final RequestModel requestModel) {}

    @Before(value = "boundaryMethods(context, requestModel)",
            argNames = "context,requestModel,thisJoinPoint")
    public void beforeBoundaryMethod(
            final BoundaryContext context,
            final RequestModel requestModel,
            final JoinPoint thisJoinPoint) {
        final Signature signature = thisJoinPoint.getSignature();
        final SecurityProvider securityProvider = context.getSecurityProvider();

        if (!securityProvider.hasAccessTo(SecurityProvider.AccessType.BOUNDARY, signature.getName())) {
            throw new RuntimeException(
                    String.format("User {%s} don't have access to boundary '%s'",
                            context.getCurrentUser().getName(),
                            signature.getName()));
        }

    }
}
